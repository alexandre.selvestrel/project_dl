import requests
import os
import bs4
import time  # Added to use sleep for retry delay

def download_image(url, directory):
    '''Save an image from a URL to a new directory.'''
    image = requests.get(url)
    if image.status_code == 200:
        filetype = image.headers['content-type'].split('/')[-1]
        name = directory + "/" + url.split("?")[1] + "." + filetype 

        file = open(name, 'wb')
        file.write(image.content)
        file.close()

def get_image(url):
    '''Get the image from a url.'''
    imagePage = requests.get(url)
    if imagePage.status_code == 200:
        imageSoup = bs4.BeautifulSoup(imagePage.text, 'html.parser')
        image = imageSoup.find(id='image')
        if image is not None:
            return image.get('src')
    return None

def get_image_links(page, url, pagecount):
    '''Get all image links on a page.'''
    print("Getting image links for Page " + str(pagecount) + "...")
    links = page.find_all('a', href=True)
    imageLinks = [url + link['href'] for link in links if not link.find('img') is None]
    return imageLinks

def get_next_page(page, url, pagecount, userSearch):
    '''Get the next page. Returns None if it is the last page.'''
    nextSoup = page.find('a', alt="next")
    if nextSoup is None:
        return None
    else:
        nextUrl = url + 'index.php?page=post&s=list&tags=' + userSearch + '&pid=' + str(pagecount*40)
    return requests.get(nextUrl)

def main(n_image, start_page=1):
    url = 'http://safebooru.org/'
    pagecount = start_page

    #userSearch = input("What tags are you searching for?\nEnter them all separated by spaces!\n\n")
    userSearch = '1boy'
    userURL = 'http://safebooru.org/index.php?page=post&s=list&tags=' + userSearch + '&pid=' + str(pagecount*40)

    path = "data/" + userSearch[:-1] + 't'
    os.makedirs(path, exist_ok=True)  # store images in a directory named after search 

    imagescount = 0
      # Use the specified starting page
    res = requests.get(userURL)  # page 1 
    begin = True

    if "Nothing found" in res.text:
        print("No images found, check your tags?")
        return

    while imagescount < n_image:
        try:
            soup = bs4.BeautifulSoup(res.text, 'html.parser')
            imageLinks = get_image_links(soup, url, pagecount)  # array of links to images
            imagescount += len(imageLinks)

            print("Getting images, this might take a while...")
            images = [get_image(link) for link in imageLinks]  # array of image links 

            for i in range(images.count(None)):
                images.remove(None)

            for imageLink in images:
                print("Downloading image: " + str(imageLink))
                download_image(imageLink, path)
                print("Done!")

            # Next Page 
            res = get_next_page(soup, url, pagecount, userSearch)
            if res is None:
                print("Finished! Downloaded " + str(imagescount) + " images!")
                return(pagecount)
                break
        except requests.RequestException as e:
            print(f"Error: {e}")
            print("Retrying...")
            time.sleep(5)  # Wait for 5 seconds before retrying

        pagecount += 1  # Increment the page count
        return pagecount

if __name__ == "__main__":
    ancient_page_count = 1000
    while len(os.listdir('./data/1bot'))< 100:
        new_pagecount = main(1, start_page=ancient_page_count)  # Start downloading from page 20
        ancient_page_count = new_pagecount #+ 1
        print(len(os.listdir('./data/1bot')))
        
    
