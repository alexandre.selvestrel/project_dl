from PIL import Image, ImageFilter
import cv2
import numpy as np
import os

'''
dir = "./data/1boy_resized"
os.listdir(dir)
img_name = os.listdir(dir)[2]
'''

def detect_edges(image_folder,image_name):
    image_path = os.path.join(image_folder, image_name)
    # Charger l'image
    original_image = Image.open(image_path)
    if original_image.mode != 'RGB':
        original_image = original_image.convert('RGB')

    # Effectuer la détection de bords avec le filtre de Sobel
    edges_image = original_image.filter(ImageFilter.FIND_EDGES)

    # Convertir l'image en niveaux de gris avec OpenCV
    edges_array = np.array(edges_image)
    gray_edges = cv2.cvtColor(edges_array, cv2.COLOR_RGB2GRAY)
    Image.fromarray(cv2.cvtColor(gray_edges, cv2.COLOR_GRAY2RGB)).save('./data/1boy_gret/grey_' + image_name)

def to_color(image_folder,image_name):
    image_path = os.path.join(image_folder, image_name)
    # Charger l'image
    colored_image = Image.open(image_path)
    if colored_image.mode != 'RGB':
        colored_image = colored_image.convert('RGB')
    return colored_image


# Appeler la fonction avec le chemin de votre image en entrée
if __name__ == '__main__':
    dir = "./data/1boy_resizet"
    liste = os.listdir(dir)
    for name in liste:
        detect_edges(dir,name)



