from PIL import Image
import os

def resize_images(input_directory, output_directory, target_size=(256, 256)):
    # Vérifier si le répertoire d'entrée existe
    if not os.path.exists(input_directory):
        print(f"Le répertoire {input_directory} n'existe pas.")
        return
    
    # Créer le répertoire de sortie s'il n'existe pas
    os.makedirs(output_directory, exist_ok=True)
    print(len(os.listdir(input_directory)))
    # Parcourir tous les fichiers dans le répertoire d'entrée
    for filename in os.listdir(input_directory):
        input_file_path = os.path.join(input_directory, filename)
        output_file_path = os.path.join(output_directory, filename)

            # Ouvrir l'image
        try:
            with Image.open(input_file_path) as img:
                    # Redimensionner l'image
                resized_img = img.resize(target_size)

                    # Sauvegarder la nouvelle image redimensionnée dans le répertoire de sortie
                resized_img.save(output_file_path)

                print(f"Image {filename} redimensionnée avec succès.")
        except Exception as e:
                print(f"Erreur lors du traitement de l'image {filename}: {str(e)}")

# Spécifiez le chemin du répertoire où se trouvent vos images d'origine
input_directory = "./data/1bot"


# Spécifiez le chemin du nouveau répertoire pour les images redimensionnées
output_directory = "./data/1boy_resizet"

# Redimensionnez les images du répertoire d'entrée et enregistrez-les dans le répertoire de sortie
resize_images(input_directory, output_directory)