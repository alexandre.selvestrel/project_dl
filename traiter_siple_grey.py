from PIL import Image, ImageFilter
import cv2
import numpy as np
import os


'''dir = "./data/1boy_resized"
print(len(os.listdir("./data/1boy_simple_grey")))'''



def simple_grey(image_folder,image_name):
    image_path = os.path.join(image_folder, image_name)
    # Charger l'image
    original_image = Image.open(image_path)
    if original_image.mode != 'RGB':
        original_image = original_image.convert('RGB')

    # Effectuer la détection de bords avec le filtre de Sobel

    # Convertir l'image en niveaux de gris avec OpenCV
    array = np.array(original_image)
    gray_edges = cv2.cvtColor(array, cv2.COLOR_RGB2GRAY)
    Image.fromarray(cv2.cvtColor(gray_edges, cv2.COLOR_GRAY2RGB)).save('./data/1boy_simple_gret/simple_grey_' + image_name)

# Appeler la fonction avec le chemin de votre image en entrée

if __name__ == '__main__':
    dir = "./data/1boy_resizet"
    liste = os.listdir(dir)
    for name in liste:
        simple_grey(dir,name)


