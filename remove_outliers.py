from PIL import Image, ImageFilter
import cv2
import numpy as np
import os

dir_1 = "./data/1boy_resized"
liste_1 = os.listdir(dir_1)
dir_2 = "./data/1boy_grey"
liste_2 = os.listdir(dir_2)
dir_3 = "./data/1boy_simple_grey"
liste_3 = os.listdir(dir_3)

count = 0

for i, elem in enumerate(liste_1):
    token_1 = liste_1[i][:]
    token_2 = liste_2[i][5:]
    token_3 = liste_3[i][12:]
    if (token_1 != token_2) or (token_2 != token_3):
        print(False,token_1,token_2,token_3)
        break
    image_path = os.path.join(dir_1, liste_1[i])
    colored_image = Image.open(image_path)
    if colored_image.mode != 'RGB':
        colored_image.close()
        count +=1
        img_1 = os.path.join(dir_1, liste_1[i])
        img_2 = os.path.join(dir_2, liste_2[i])
        img_3 = os.path.join(dir_3, liste_3[i])
        os.remove(img_1)
        os.remove(img_2)
        os.remove(img_3)


print('end',len(liste_1))
print('outliers',count)

